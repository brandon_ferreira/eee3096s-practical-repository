//---------------------------------------------------------------
// Author: Brandon Ferreira and Lezerick Owies
// Originally made use of UCT Tutorial 1 code.
//
// DESCRIPTION: This code is for the submission of Tutorial 1 of 
// EEE3096S offered by UCT
//
// Last editted: 2021/08/13
//--------------------------------------------------------------
# include <stdio.h>
// Main function

int main(){
	int a, b, sum;		// Variable initialization
	// Prompts the user for the value of a
	printf("Enter a value for a: ");
	scanf("%d", &a);
	// Prompts the user for the value of b
	printf("Enter a value for b: ");
	scanf("%d", &b);
	// Sums the value of a and b and displays the result
	sum = a + b;

	printf("The sum of a and b is %d \n.",sum);
}
