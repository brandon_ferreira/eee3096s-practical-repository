#------------------------------------------------------------------------------
# Import libraries
#------------------------------------------------------------------------------
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os
#------------------------------------------------------------------------------
# some global variables that need to change as we run the program
end_of_game = None  # set if the user wins or ends the game
#------------------------------------------------------------------------------
# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]  # Defines the LED pins
LED_accuracy = 32         # Defines Accuracy LED pin
btn_submit = 16           # Defines submit button pin
btn_increase = 18         # Defines increase button pin
buzzer = None             # Not used
eeprom = ES2EEPROMUtils.ES2EEPROM()  # Initializes the eeprom
# OUR ADDED VARIABLES
number = 0                # Used to store amount of guesses
led_pwm = 0               # Used to control LED PWM
buz_pwm = 0               # Used to control Buzzer PWM

value = 0                 # Used to store generated number
scores = []               # Used to hold read in scores
score_count = 0           # Used to hold how many scores
max_size = 16             # Maximum scores allowed 
newScore = []             # Used to hold the nex score value

name = [0,0,0]            # For name formatting
gameState = 0             # Used to determine whether game is over or not
guesses = 0               # Holds number guessed made

#----------------------------------------------------------------
# Welcome function
# Description: Print the game banner
#----------------------------------------------------------------
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")

#-----------------------------------------------------------------
# Menu function
# Description: Print the game menu
#-----------------------------------------------------------------
def menu():
    # Defining which global variables to use
    global end_of_game
    global value
    global newScore
    # Option menu: User selects what they want to do
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    # Displays the 3 highest scores as well as how many scores are stored
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        #newScore = [ord('H'),ord('A'),ord('H'),2]
        #save_scores()
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    # Starts a new game
    elif option == "P":
        end_of_game=None
        guesses = 0

        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        value = generate_number()
        #print("The random number is")
        #print(value)
        #print("User name is : {}{}{}".format(name[0],name[1],name[2]))
        while not end_of_game:
            pass
    # Exits the game
    elif option == "Q":
        print("Come back soon!")
        exit()
    # Handles invalid enteries
    else:
        print("Invalid option. Please select a valid one!")

#---------------------------------------------------------------
# display_scores
# Description: displays the 3 highest scores on the screen as well
#              as how many scores are stored
#---------------------------------------------------------------
def display_scores(count, raw_data):
    # print the scores to the screen in the expected format
    print("There are {} scores. Here are the top 3!".format(count))
    # print out the scores in the required format
    #data_to_read = eeprom.read_block(1,4)
    data_to_read = raw_data[1]
    print("1 - {}{}{} took {} guesses".format(chr(data_to_read[0]),chr( data_to_read[1]),chr(data_to_read[2]),data_to_read[3]))
    #data_to_read = eeprom.read_block(2,4)
    data_to_read = raw_data[2]
    print("2 - {}{}{} took {} guesses".format(chr(data_to_read[0]),chr( data_to_read[1]),chr(data_to_read[2]),data_to_read[3]))
    #data_to_read = eeprom.read_block(3,4)
    data_to_read = raw_data[3]
    print("3 - {}{}{} took {} guesses".format(chr(data_to_read[0]),chr( data_to_read[1]),chr(data_to_read[2]),data_to_read[3]))
    pass
#------------------------------------------------------------------------
# Setup Pins: This function initializes all the necessary GPIO's
#------------------------------------------------------------------------
def setup():
    # Defining which global variables to use
    global led_pwm
    global buz_pwm
    # Setup board mode
    GPIO.setmode(GPIO.BOARD)
    # Setup regular GPIO
    GPIO.setup(LED_value[0],GPIO.OUT)
    GPIO.setup(LED_value[1],GPIO.OUT)
    GPIO.setup(LED_value[2],GPIO.OUT)
    GPIO.setup(btn_submit,GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.setup(btn_increase,GPIO.IN,pull_up_down=GPIO.PUD_UP)

    GPIO.output(11,GPIO.LOW)
    GPIO.output(13,GPIO.LOW)
    GPIO.output(15,GPIO.LOW)

    # Setup PWM channels
    GPIO.setup(LED_accuracy,GPIO.OUT)
    led_pwm=GPIO.PWM(LED_accuracy,100)
    led_pwm.start(0)

    GPIO.setup(33,GPIO.OUT)
    buz_pwm=GPIO.PWM(33,1)
    buz_pwm.start(0)
    # Setup debouncing and callbacks
    GPIO.add_event_detect(btn_submit,GPIO.FALLING,callback=btn_guess_pressed,bouncetime=400)
    GPIO.add_event_detect(btn_increase,GPIO.FALLING,callback=btn_increase_pressed,bouncetime=300)
    pass

#---------------------------------------------------------------------------
# Load high scores by retrieving them from the eeprom
#---------------------------------------------------------------------------
def fetch_scores():
    # Defining which global variables to use
    global data_in
    global scores
    global score_count
    global eeprom
    # get however many scores there are
    score_count = 0
    scores = []
    # Get the scores
    #eeprom.populate_mock_scores()
    #eeprom.clear(4096)

    #newScore = [ord('D'),ord('S'),ord('P'),2]
    #save_scores()

    i=0
    while(i<10000):
        i=i+1
    temp = eeprom.read_block(0,4)
    score_count = eeprom.read_byte(0)
    i=0
    while(i<1000):
        i=i+1
    i=0
    scores.append(temp)
    while(i<score_count):
        temp = eeprom.read_block(i+1,4)
        z = 0
        data_to_read = [temp[0],temp[1],temp[2],temp[3]]
        while(z<1000):
            z=z+1
        scores.append(data_to_read)
        i=i+1
    #while(i<1000):
    #    i=i+1
    # Convert Code back to ascii
    # return back the results
    return score_count, scores

#-----------------------------------------------------------------
# Save high scores to the data eeprom
#-----------------------------------------------------------------
def save_scores():
    # Define what global variables to use
    global scores
    global score_count
    global newScore
    global eeprom

    #Fetch the scores
    score_count,scores = fetch_scores()
    # include new score
    # If the eeprom is full, this replaces the lowest one
    if (score_count>15):
        scores[15] = newScore
        i = 0
        #eeprom.clear(4096)
        scores.sort(key=lambda x: x[3])
        x=0
        while(x<15):
            eeprom.write_block(x+1,scores[x+1])
            z=0
            while(z<2000):
                z=z+1
            x=x+1
    # Adds new score to eeprom
    else:
        scores.append(newScore)
        scores.sort(key=lambda x: x[3])
        x=0
        while(x<1000):
            x=x+1
        score_count = score_count+1
        eeprom.write_byte(0,score_count)
        x=0
        while(x<score_count):
            eeprom.write_block(x+1,scores[x+1])
            z=0
            while(z<10000):
                z=z+1
            x=x+1
    # sort
    # update total amount of scores
    # write new scores
    pass

#--------------------------------------------------------
# Generate guess number
#--------------------------------------------------------
def generate_number():
    return random.randint(0, pow(2, 3)-1)

#--------------------------------------------------------
# Increase button pressed: Interrupt that handles the LED
# increasing action and increments users guess value
#--------------------------------------------------------
def btn_increase_pressed(channel):
    # GPIO.output(LED_value[0],GPIO.HIGH)
	global number
	number = number + 1
	if number==0:
		GPIO.output(LED_value[0],GPIO.LOW)
		GPIO.output(LED_value[1],GPIO.LOW)
		GPIO.output(LED_value[2],GPIO.LOW)
	elif number==1:
		GPIO.output(LED_value[0],GPIO.HIGH)
		GPIO.output(LED_value[1],GPIO.LOW)
		GPIO.output(LED_value[2],GPIO.LOW)
	elif number==2:
		GPIO.output(LED_value[0],GPIO.LOW)
		GPIO.output(LED_value[1],GPIO.HIGH)
		GPIO.output(LED_value[2],GPIO.LOW)
	elif number==3:
		GPIO.output(LED_value[0],GPIO.HIGH)
		GPIO.output(LED_value[1],GPIO.HIGH)
		GPIO.output(LED_value[2],GPIO.LOW)
	elif number==4:
		GPIO.output(LED_value[0],GPIO.LOW)
		GPIO.output(LED_value[1],GPIO.LOW)
		GPIO.output(LED_value[2],GPIO.HIGH)
	elif number==5:
		GPIO.output(LED_value[0],GPIO.HIGH)
		GPIO.output(LED_value[1],GPIO.LOW)
		GPIO.output(LED_value[2],GPIO.HIGH)
	elif number==6:
		GPIO.output(LED_value[0],GPIO.LOW)
		GPIO.output(LED_value[1],GPIO.HIGH)
		GPIO.output(LED_value[2],GPIO.HIGH)
	elif number==7:
		GPIO.output(LED_value[0],GPIO.HIGH)
		GPIO.output(LED_value[1],GPIO.HIGH)
		GPIO.output(LED_value[2],GPIO.HIGH)

	#number = number+1
	if (number > 7):
		number = 0
		GPIO.output(LED_value[0],GPIO.LOW)
		GPIO.output(LED_value[1],GPIO.LOW)
		GPIO.output(LED_value[2],GPIO.LOW)
	return number
	#number=number+1
	#if(number>7):
	#	number=0
    # Increase the value shown on the LEDs
pass

#----------------------------------------------------------
# Guess button: Submits the user's guess value to be checked
# if it is correct, trigger a game over. If incorrect, increment
# guess count and make necessary changes to PWM LED and buzzer
#----------------------------------------------------------
def btn_guess_pressed(channel):
    global value
    global number
    global name
    global end_of_game
    global newScore
    global guesses
    # Compare the actual value with the user value displayed on the LED
    guesses = guesses+1
    accuracy_leds()
    trigger_buzzer()
    tempStr = 0
    # If guess is correct, prompts user to enter their name and ends the game
    if(value==number):
        #tempStr = input("Please enter your name: ")
        while(tempStr==0):
            tempStr = input("Please enter your name: ")
        #end_of_game = True
        name = ['N','o','n']
        newScore = [0,0,0,0]
        i=0

        for Str in tempStr:
           name[i] = Str
           newScore[i] = ord(Str)
           i=i+1
           if(i>2):
               break
        newScore[3] = guesses
        save_scores()
        end_of_game = True
    pass

#----------------------------------------------------------------
# LED Brightness: Adjusts the LED brightness with PWM based on how
# close the user is to the guess. The brighter the LED, the closer
# the guess
#----------------------------------------------------------------
def accuracy_leds():
    global led_pwm
    global number
    global value
    # Set the brightness of the LED based on how close the guess is to the answer
    #print("User number is")
    #print(number)
    temp = ((8-abs(value-number))/8)*100
    led_pwm.ChangeDutyCycle(temp)
    if (value==number):
        led_pwm.ChangeDutyCycle(0)
    pass
#------------------------------------------------------------------
# Sound Buzzer: Adjusts the buzzer frequency based on how close the
# user is to the answer. The buzzer beeps more frequenctly the closer
# the user gets.
#------------------------------------------------------------------
def trigger_buzzer():
    # Define global variables to be used
    global value
    global number
    global buz_pwm
    # Set duty cycle to 50%
    buz_pwm.ChangeDutyCycle(50)
    # If the user is off by an absolute value of 3, the buzzer should sound once every second
    # If the user is off by an absolute value of 2, the buzzer should sound twice every second
    # If the user is off by an absolute value of 1, the buzzer should sound 4 times a second
    if (value==number):
        buz_pwm.ChangeDutyCycle(0)
    elif (number>value):
        temp = 8-number
        if (abs(value-number)==1):
            buz_pwm.ChangeFrequency(4)
        elif (abs(value-number)==2):
            buz_pwm.ChangeFrequency(2)
        elif (abs(value-number)==3):
            buz_pwm.ChangeFrequency(1)
        else:
            buz_pwm.ChangeFrequency(1)
    else:
        if (abs(value-number)==1):
            buz_pwm.ChangeFrequency(4)
        elif (abs(value-number)==2):
            buz_pwm.ChangeFrequency(2)
        elif (abs(value-number)==3):
            buz_pwm.ChangeFrequency(1)
        else:
            buz_pwm.ChangeFrequency(1)
    pass

#-----------------------------------------------------------------
# Main method to run program
#-----------------------------------------------------------------
if __name__ == "__main__":
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.remove_event_detect(btn_submit)
        GPIO.remove_event_detect(btn_increase)
        GPIO.cleanup()
#------------------------------------------------------------------
# End of code
#------------------------------------------------------------------
