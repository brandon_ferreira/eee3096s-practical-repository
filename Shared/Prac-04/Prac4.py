#--------------------------------------------------------------------
# Practical 4 of EEE3096S
# Authors: Brandon Ferreira and Lezerick Owies
# Student numbers: FRRBRA002 and OWSLEZ001
#--------------------------------------------------------------------
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn

import threading
import time

import RPi.GPIO as GPIO
#---------------------------------------------------------------------
# Communication Variable set-up
#---------------------------------------------------------------------
# Creates the SPI bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
# Creates the CS (Chip Select)
cs = digitalio.DigitalInOut(board.D5)
# Create MCP object
mcp = MCP.MCP3008(spi,cs)
# Button pin
btn_timer = 16
#---------------------------------------------------------------------
# Data Variable Set-up
#---------------------------------------------------------------------
temp_data = 0
light_data = 0

current_time = 0
display_timer = 1
prev_timer = 10

start_flag = 1
change_flag = 0
# Create an analog input channel on pin 0
# chan = AnalogIn(mcp, MCP.P1)

def disp():
    # Allows accessing of global variables
    global current_time
    global display_timer
    global change_flag
    global start_flag
    #-------------------------------------------------------------
    # Creates and starts a Thread that activates based on a timer
    # The timing of the thread is determined by the global variable
    # display_timer. This will run until the program is stopped
    #-------------------------------------------------------------
    thread = threading.Timer(display_timer,disp)
    thread.daemon = True
    thread.start()
    #-------------------------------------------------------------
    # Reads data in from the ADC pins
    #-------------------------------------------------------------
    chan = AnalogIn(mcp,MCP.P1)
    temp_data = chan.voltage
    # Manipulating voltage for temperature sensor to display actual
    # Temperature. (See report and data sheet for equation)
    temp_data_2 = temp_data - 0.5
    temp_data_2 = temp_data_2/0.01
    temp_data = chan.value
    # Reads the LDR data
    chan = AnalogIn(mcp,MCP.P2)
    light_data = chan.value
    #-------------------------------------------------------------
    # Updates the current run-time
    #-------------------------------------------------------------
    if (start_flag == 0):
        if (change_flag == 1):
            current_time = current_time+prev_timer
            change_flag = 0
        else:
            current_time = current_time+display_timer
    else:
        current_time = 0
        start_flag = 0
    #-------------------------------------------------------------
    # Updates the display in the correct format
    #-------------------------------------------------------------
    if (current_time>=10 and current_time<100):
        print('{}s{:>19}{:>20} C{:>30}'.format(current_time,temp_data,round(temp_data_2,3),light_data))
    elif (current_time>=100):
        print('{}s{:>18}{:>20} C{:>30}'.format(current_time,temp_data,round(temp_data_2,3),light_data))
    else:
        print('{}s{:>20}{:>20} C{:>30}'.format(current_time,temp_data,round(temp_data_2,3),light_data))

    pass
#-------------------------------------------------------------------
# Button Interrupt Function
#-------------------------------------------------------------------
def incTimer(channel):
    #-------------------------------------------------------------
    # Allows access to global variables listed
    #-------------------------------------------------------------
    global display_timer
    global change_flag
    global prev_timer
    #-------------------------------------------------------------
    # Changes the timing delay on the thread for the display
    #-------------------------------------------------------------
    if (display_timer == 1):
       display_timer = 5
       prev_timer = 1
       change_flag = 1
    elif (display_timer == 5):
       display_timer = 10
       prev_timer = 5
       change_flag = 1
    else:
       display_timer = 1
       prev_timer = 10
       change_flag = 1
    pass
#-------------------------------------------------------------------
# GPIO Set-Up
#-------------------------------------------------------------------
def setup():
    #-------------------------------------------------------------
    # Sets up the GPIO for a button input as well as links that
    # button to a function that will be triggered as an interrupt
    #-------------------------------------------------------------
    GPIO.setup(btn_timer,GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(btn_timer,GPIO.FALLING,callback=incTimer,bouncetime=300)
    pass
#-------------------------------------------------------------------
# Start-up Routine
#-------------------------------------------------------------------
def main():
    print('Runtime          Temp Reading         Temp                           Light Reading')
    setup()
    disp()
    while True:
        pass

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Exiting")
    except Exception as e:
        print('Error')
#-------------------------------------------------------------------
# End Of Program
#-------------------------------------------------------------------

